package rlpString

import (
	"awesomeProject/goRlp/utils"
	"hash/fnv"
	"math/big"

)

type RlpString struct {
	value []byte
}

func NewRlpString(value []byte) *RlpString {
	return &RlpString{value: value}
}

func CreateRlpString(value string) *RlpString {
	return NewRlpString([]byte(value))
}

func CreateRlpStringBigInt(value *big.Int) *RlpString {
	if value.Sign() <= 0 {
		return NewRlpString([]byte{})
	} else {
		var bytes = value.Bytes()
		if int(bytes[0]) == 0 { // remove leading zero
			return NewRlpString(bytes[1:])
		} else {
			return NewRlpString(bytes)
		}
	}
}

func hash(s string) uint32 {
	h := fnv.New32a() //h is declared for the first time. //// New32a returns a new 32-bit FNV-1a hash.Hash.
	h.Write([]byte(s))
	return h.Sum32() //
}

func (r *RlpString) AsString() string {
	encodedStr := utils.ToHexStringByte(r.value) //encodedStr is declared for the first time.
	return encodedStr
}
func (r *RlpString) Hash() uint32 {
	str := r.AsString() // str is declared for the first time.
	return hash(str)
}

func (r *RlpString) GetBytes() []byte {
	return r.value
}
